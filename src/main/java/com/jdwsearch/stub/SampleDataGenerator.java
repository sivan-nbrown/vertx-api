package com.jdwsearch.stub;

import com.jdwsearch.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SampleDataGenerator {
    public static List<Product> getSampleData() {

        Random random = new Random();

        double minMargin = 5;
        double maxMargin = 300;

        List<Product> products = Arrays.asList(
                new Product(
                        1,
                        "Luna Lace Up Slim Leg Jeans",
                        "https://i1.adis.ws/i/simplybe/q02sx849502w_82649fb8/SX849.jpeg?$plp-320$",
                        false,
                        46.99),
                new Product(
                        2,
                        "Fern Distressed Boyfriend Jeans",
                        "https://i1.adis.ws/i/simplybe/q02sx652501w_8264a38d/SX652.jpeg?$plp-320$",
                        false,
                        40.49),
                new Product(
                        3,
                        "Joss Frayed Low Hem Cropped Wide Leg Jeans",
                        "https://i1.adis.ws/i/simplybe/q02sx849502w_82649fb8/SX849.jpeg?$plp-320$",
                        false,
                        46.99),
                new Product(
                        4,
                        "Shape & Sculpt High Waist Straight Leg",
                        "https://i1.adis.ws/i/simplybe/q02oq996500w_8264ab80/OQ996.jpeg?$plp-320$",
                        false,
                        46.99),
                new Product(
                        5,
                        "Chloe Chewed Low Hem High Waist Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/q02sx956504w_8264ac69/SX956.jpeg?$plp-320$",
                        false,
                        32.99),
                new Product(
                        6,
                        "Sophia Supersoft Fly Front Jeggings",
                        "https://i1.adis.ws/i/simplybe/o02vz279502w_81ed1c7d/VZ279.jpeg?$pdp-720$",
                        false,
                        35.99),
                new Product(
                        7,
                        "Value Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/p02dj016500a_8221b1d2/DJ016.jpeg?$pdp-720$",
                        false,
                        41.99),
                new Product(
                        8,
                        "Chloe Distressed Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/o02vz222503w_81e8fbbd/VZ222.jpeg?$pdp-720$",
                        false,
                        22.99),
                new Product(
                        9,
                        "Chloe Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/o02vz020506w_81f010e8/VZ020.jpeg?$pdp-720$",
                        false,
                        55.99),
                new Product(
                        10,
                        "Lexi High Waist Slim Leg Jeans",
                        "https://i1.adis.ws/i/simplybe/q02et029501a_826c9699/ET029.jpeg?$pdp-720$",
                        false,
                        44.99),
                new Product(
                        11,
                        "Joe Browns Remarkable Applique Jeans",
                        "https://i1.adis.ws/i/simplybe/q02lg617500w_826bc54e/LG617.jpeg?$pdp-720$",
                        false,
                        88.99),
                new Product(
                        12,
                        "Junarose Queen Skinny Jean",
                        "https://i1.adis.ws/i/simplybe/q02bl962714d_8280b13d/BL962.jpeg?$pdp-720$",
                        false,
                        35.99),
                new Product(
                        13,
                        "Joe Browns Floral Embroidered Jeans",
                        "https://i1.adis.ws/i/simplybe/q02lg627501w_82837d8b/LG627.jpeg?$pdp-720$",
                        false,
                        49.99),
                new Product(
                        14,
                        "Levi S 311 Shaping Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/q02yq550503a_8278c144/YQ552.jpeg?$pdp-720$",
                        false,
                        54.99),
                new Product(
                        15,
                        "Joanna Hope Embellished Jeans",
                        "https://i1.adis.ws/i/simplybe/q02ar274710w_827c4c52/AR274.jpeg?$pdp-720$",
                        false,
                        39.99),
                new Product(
                        16,
                        "Premium Shape & Sculpt High Waisted Straight Leg Jeans",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02op171500w_826cb4a6/OP171" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        38.99),
                new Product(
                        17,
                        "Amber Pull-On Skinny Jeggings",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02op145504w_826c647f/OP145" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        30.99),
                new Product(
                        18,
                        "Erin Pull-On Bootcut Jeggings",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/o02op146504w_81f01636/OP146" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        31.99),
                new Product(
                        19,
                        "Chloe Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/o02vz020506w_81f010e8/VZ020" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        40.99),
                new Product(
                        20,
                        "Joe Browns Awesome Slim Leg Jeans",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/i04hx470500c_8180da8c/HX470" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        42.99),
                new Product(
                        21,
                        "Chloe Distressed Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/o02vz087503w_81ecd5fe/VZ087" +
                        ".jpeg&$plp-320$]&layer1=[src=/i/simplybe/161212_sbu_badge_sale&anchor=BL&left=15&bottom=15&$badge-320$]&$plp-320$&$plp-320-qlt$",
                        true,
                        46.99),
                new Product(
                        22,
                        "Lovedrobe Off-the-Shoulder Flare Sleeve Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi508704w_828fc32e/YI508.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        59.99),
                new Product(
                        23,
                        "Apricot Parrott Print Zipper Front Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi660708w_8290d15c/YI660.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        32.49),
                new Product(
                        24,
                        "Apricot Stripe Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi667705s_828f45af/YI667.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        24.49),
                new Product(
                        25,
                        "Joe Browns Free Flowing Floral Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yg168706w_82875937/YG168.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        41.99),
                new Product(
                        26,
                        "Vero Moda Sleeveless Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi749704w_8290ce5e/YI749.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        37.49),
                new Product(
                        27,
                        "Vero Moda Easy Off-the-Shoulder Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ln040705w_8290cb10/LN040.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        29.49),
                new Product(
                        28,
                        "Sweetheart Short Sleeve Off-the-Shoulder Sweater",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02is131704w_828e9ca3/IS131.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        29,
                        "Layer Frill Kimono",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ar490708w_8288683c/AR490.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        49.49),
                new Product(
                        30,
                        "Neon Rose Patchwork Maxi Smock Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02bl032704s_828f92ff/BL032.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        59.99),
                new Product(
                        31,
                        "Vero Moda Printed Wide Leg Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi768706w_8290ceaf/YI768.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        52.49),
                new Product(
                        32,
                        "Zipper Front Denim Pinafore",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02vh505707w_8289afe0/VH505.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        40.49),
                new Product(
                        33,
                        "Print Shirred Waist Tapered Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fd477716w_828ce71c/FD477.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        40.49),
                new Product(
                        34,
                        "Simply Be Deep V Neck T Shirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02hx666713w_827e4fef/HX666.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        14.99),
                new Product(
                        35,
                        "Sweetheart Short Sleeve Off-the-Shoulder Sweater",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02is134705w_828e9b7e/IS134.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        36,
                        "Vero Moda Short Print Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi760707w_8290cef5/YI760.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        54.99),
                new Product(
                        37,
                        "Lightweight Boyfriend T-shirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr710705s_828b5b9f/DR710.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        17.49),
                new Product(
                        38,
                        "Infinity 4 Way Stretch Skinny Jeans",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02vz621705w_82885659/VZ621.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        59.99),
                new Product(
                        39,
                        "Print Button Front Culottes",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fh424706w_828a3c12/FH424.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        40.49),
                new Product(
                        40,
                        "Floral Crinkle Shirred Maxi Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02gb555706w_8287d749/GB555.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        49.99),
                new Product(
                        41,
                        "Neon Lilac Burnout Slogan Tank  Top Tunic",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr059706w_828bbd26/DR059.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        20.49),
                new Product(
                        42,
                        "Sequin Lips Embellished T Shirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr736714w_828b5bf8/DR736.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        43,
                        "Pack of 2 Boobtubes",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01dr676718w_828a501f/DR676.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        17.49),
                new Product(
                        44,
                        "Simply Be Value Ribbed Tank  Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr731706w_82908881/DR731.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        14.49),
                new Product(
                        45,
                        "Border Print Side Piped Wide Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02cr035705w_828a448c/CR035.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        40.49),
                new Product(
                        46,
                        "Bright Sequin Lips Tank  Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr366707w_828b5262/DR366.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        31.99),
                new Product(
                        47,
                        "Longline Kimono",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ar486706w_8283c0ad/AR486.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        31.99),
                new Product(
                        48,
                        "Ruffle Boxy Tee",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yn456707w_82822493/YN456.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        49,
                        "Split Wide Leg Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fh146710w_828aa6f9/FH146.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        50,
                        "Print Crinkle Wrap Crop Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01fk068710s_828ce7df/FK068.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        43.49),
                new Product(
                        51,
                        "Quiz Off-the-Shoulder Spot Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em198501w_8273e759/EM198.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        38.99),
                new Product(
                        52,
                        "Ruffle Boxy Tee",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yn447707w_82822080/YN447.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        53,
                        "Print Curved Low Hem Culottes Floral",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fh396710w_828876f6/FH396.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        54,
                        "Lovedrobe Printed Kimono",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em794704w_828a5178/EM794.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        64.99),
                new Product(
                        55,
                        "Tencel Wrap Culottes Jumpsuit",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02tc746706w_828855fa/TC746.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        54.99),
                new Product(
                        56,
                        "WIDE LEG KIMONO JUMPSUIT",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/p02gl265500w_824e1542/GL265.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        46.99),
                new Product(
                        57,
                        "Quiz Cobalt Cullote Jumpsuit",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em426501w_82788f4f/EM426.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        58,
                        "Embroidered Low Hem Tencel Cami Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02sx662705w_82885556/SX662.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        33.49),
                new Product(
                        59,
                        "Linen Mix Shorts",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ku027704w_828a3dee/KU027.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        21.49),
                new Product(
                        60,
                        "Wrap Kimono Chiffon Blouse",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ar466706w_828eaecc/AR466.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        34.99),
                new Product(
                        61,
                        "African Print Patchwork Mom Shorts",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02vh696707w_828e0423/VH696.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        40.49),
                new Product(
                        62,
                        "Floral Print Jersey Shorts",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02cr287707w_828dd162/CR287.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        20.49),
                new Product(
                        63,
                        "Print Crinkle Shirred Waist Maxi Skirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fk067705w_828a335d/FK067.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        46.49),
                new Product(
                        64,
                        "Quiz Floral Off-the-Shoulder Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em500712d_8280b279/EM500.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        35.99),
                new Product(
                        65,
                        "Figleaves Curve Adore Chemise",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02tk681500w_82816a44/TK681.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        52.49),
                new Product(
                        66,
                        "Black Floral Scuba Wrap Asymmetric Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02kq959706w_828129a0/KQ959.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        31.99),
                new Product(
                        67,
                        "Simply Be Kimono",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02bk972501w_8265662d/BK972.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        48.49),
                new Product(
                        68,
                        "African Print Pussy Bow Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02gb133704w_828bd5f6/GB133.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        41.99),
                new Product(
                        69,
                        "Lovedrobe Embroidered Lace Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em522706w_8289bad9/EM522.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        79.99),
                new Product(
                        70,
                        "Dorina Romy Modal Blend Camisole",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dh698705w_8290d6d8/DH698.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        17.49),
                new Product(
                        71,
                        "Ann Summers Azealia Hi Waist Briefs",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/p02ft397501w_8263a33e/FT397.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        31.99),
                new Product(
                        72,
                        "Neon Rose Tye Dye Midi Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02em819706w_8289bd4a/EM819.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        46.49),
                new Product(
                        73,
                        "Red Multi Stripe Zipper Half Sleeve Rib Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr776501w_8277f787/DR776.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        74,
                        "Pretty Secrets Tie Front Jumpsuit",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ui214708w_8287badf/UI214.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        42.99),
                new Product(
                        75,
                        "Pretty Secrets Magnetic Opening Sleepshirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01bk423500s_82703f83/BK423.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        32.49),
                new Product(
                        76,
                        "Pretty Secrets Magnetic Opening PJ Set",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01bk464505s_82789383/BK464.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        37.99),
                new Product(
                        77,
                        "Side Pocket Tunic Short Sleeve",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02dr644706w_82896d6e/DR644.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        78,
                        "Oasis Curve Flash Stripe V Neck Sweater",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02ln233712d_8280b5b5/LN233.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        54.99),
                new Product(
                        79,
                        "Together Jungle Minimalist Palazzo Pant",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02hj539806w_8290ce26/HJ539.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        46.99),
                new Product(
                        80,
                        "Crochet Cardigan",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02cw651705w_828a31cb/CW651.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        72.49),
                new Product(
                        81,
                        "Sweetheart Short Sleeve Off-the-Shoulder Sweater",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yn793705w_828e9ceb/YN793.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        82,
                        "Pretty Secrets 3pk Nighties 46in",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01bk235712s_828d1d44/QY604.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        38.99),
                new Product(
                        83,
                        "Black Short Sleeve Square Neck Top",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01kr195500s_828ed0fa/KR195.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        84,
                        "Zebra Print Split Front Shirt Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02gb390708w_8284055b/GB390.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        57.49),
                new Product(
                        85,
                        "One Shoulder Textured Swimsuit",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01td229708w_8288d4fc/TD229.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        48.49),
                new Product(
                        86,
                        "Pretty Secrets 10 pack Full Fit Briefs",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q01zd557502s_82833eb2/ZD557.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        24.49),
                new Product(
                        87,
                        "Boyfriend Cardigan",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yi472500w_82755c09/YI472.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        28.99),
                new Product(
                        88,
                        "Pointelle Dress",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02yn230704w_828a995f/YN230.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        57.49),
                new Product(
                        89,
                        "Zipper Trim Tapered Soft Crepe Pants",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02fk390717w_827e6790/FK390.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        43.49),
                new Product(
                        90,
                        "Short Sleeve Printed Mesh T-Shirt",
                        "https://i1.adis.ws/i/simplybe/?layer0=[src=/i/simplybe/q02kr206706w_828ed2f4/KR206.jpeg&$plpsmall$][MEDIA-LAYER-plpsmall]&$plpsmall$&$plpsmall-qlt$",
                        false,
                        22.99));

        List<Product> productList = new ArrayList<>();

        products.forEach(product -> {
            Product item = Product.builder()
                                  .productId(product.getProductId())
                                  .description(product.getDescription())
                                  .imageUrl(product.getImageUrl())
                                  .inSale(product.isInSale())
                                  .price(product.getPrice())
                                  .qtySoldInLastDay(random.nextInt(500))
                                  .margin(minMargin + (maxMargin - minMargin) * random.nextDouble())
                                  .views(random.nextInt(10000))
                                  .stock(random.nextInt(300))
                                  .build();
            productList.add(item);
        });
        return productList;
    }
}
