package com.jdwsearch.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class HttpServerVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);

    private static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";

    private static final String CONFIG_PRODUCT_DB_QUEUE = "productdb.queue";

    private String productDbQueue = "productdb.queue";

    public void start(Future<Void> startFuture) {
        productDbQueue = config().getString(CONFIG_PRODUCT_DB_QUEUE, "productdb.queue");
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);

        router.post().handler(BodyHandler.create());
        router.patch().handler(BodyHandler.create());

        //product endpoints
        router.delete("/products").handler(this::deleteAll);
        router.get("/products").handler(this::getAll);
        router.get("/products/:site").handler(this::getAll);
        router.get("/products/:productId").handler(this::getForId);
        router.post("/products").handler(this::create);
        router.patch("/products").handler(this::update);
        router.delete("/products/:productId").handler(this::delete);

        //RankingConfig endpoint
        router.get("/ranking-config/:site").handler(this::getRankingConfigForSite);
        router.delete("/ranking-config/:site").handler(this::deleteRankingConfigForSite);
        router.post("/ranking-config").handler(this::saveRankingConfigForSite);
        router.patch("/ranking-config").handler(this::updateRankingConfigForSite);
        router.get("/ranking-config").handler(this::getAllRankingConfigs);


        int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 5000);
        server.requestHandler(router)
                .listen(
                        portNumber,
                        ar -> {
                            if (ar.succeeded()) {
                                LOGGER.info("HTTP server running on port " + portNumber);
                                startFuture.complete();
                            } else {
                                LOGGER.error("Could not start a HTTP server", ar.cause());
                                startFuture.fail(ar.cause());
                            }
                        });
    }

    private void deleteRankingConfigForSite(RoutingContext context) {
        JsonObject params = context.getBodyAsJson();
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "delete-ranking-config");
        respondToModifyOrDelete(context, params, options);
    }

    private void updateRankingConfigForSite(RoutingContext context) {
        JsonObject params = context.getBodyAsJson();
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "update-ranking-config");
        respondToModifyOrDelete(context, params, options);
    }

    private void saveRankingConfigForSite(RoutingContext context) {
        JsonObject params = context.getBodyAsJson();
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "create-ranking-config");
        vertx.eventBus()
             .send(
                     productDbQueue,
                     params,
                     options,
                     reply -> {
                         if (reply.succeeded()) {
                             setCorsHeaders(context);
                             context.response()
                                    .setStatusCode(201)
                                    .putHeader(
                                            "Location",
                                            String.format(
                                                    "/%s/%s",
                                                    "ranking-config", params.getString("site")))
                                    .end(
                                            Json.encodePrettily(
                                                    new JsonObject().put("Success", "true")));
                         } else {
                             context.fail(reply.cause());
                         }
                     });
    }

    private void getRankingConfigForSite(RoutingContext context) {
        String site = context.request().getParam("site");
        JsonObject params = new JsonObject().put("site", site);
        respondForGet(context, params, new DeliveryOptions().addHeader("action", "get-ranking-config"));
    }

    private void deleteAll(RoutingContext context) {
        respondToModifyOrDelete(context, new JsonObject(), new DeliveryOptions().addHeader("action", "delete-all"));
    }

    private void getForId(RoutingContext context) {
        String productId = context.request().getParam("productId");
        JsonObject params = new JsonObject().put("productId", Integer.parseInt(productId));
        respondForGet(context, params, new DeliveryOptions().addHeader("action", "get-product"));
    }

    private void getAll(RoutingContext context) {
        respondForGet(
                context,
                new JsonObject().put("site", context.request().getParam("site")),
                new DeliveryOptions().addHeader("action", "all-products"));
    }

    private void getAllRankingConfigs(RoutingContext context) {
        respondForGet(
                context,
                new JsonObject(),
                new DeliveryOptions().addHeader("action", "all-ranking-config"));
    }

    private void create(RoutingContext context) {
        JsonObject params = context.getBodyAsJson();
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "create-product");
        vertx.eventBus()
                .send(
                        productDbQueue,
                        params,
                        options,
                        reply -> {
                            if (reply.succeeded()) {
                                setCorsHeaders(context);
                                context.response()
                                        .setStatusCode(201)
                                        .putHeader(
                                                "Location",
                                                String.format(
                                                        "/%s/%d",
                                                        "product", params.getInteger("productId")))
                                        .end(
                                                Json.encodePrettily(
                                                        new JsonObject().put("Success", "true")));
                            } else {
                                context.fail(reply.cause());
                            }
                        });
    }

    private void delete(RoutingContext context) {
        String productId = context.request().getParam("productId");
        JsonObject params = new JsonObject().put("productId", Integer.parseInt(productId));
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "delete-product");
        respondToModifyOrDelete(context, params, options);
    }

    private void update(RoutingContext context) {
        JsonObject params = context.getBodyAsJson();
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "update-product");
        respondToModifyOrDelete(context, params, options);
    }

    private void respondToModifyOrDelete(
            RoutingContext context, JsonObject params, DeliveryOptions options) {
        vertx.eventBus()
                .send(
                        productDbQueue,
                        params,
                        options,
                        reply -> {
                            if (reply.succeeded()) {
                                setCorsHeaders(context);
                                context.response().setStatusCode(200).end("ok");
                            } else {
                                context.fail(reply.cause());
                            }
                        });
    }

    private void respondForGet(RoutingContext context, JsonObject params, DeliveryOptions options) {
        vertx.eventBus()
                .send(
                        productDbQueue,
                        params,
                        options,
                        reply -> {
                            if (reply.succeeded()) {
                                JsonObject body = (JsonObject) reply.result().body();
                                setResponseForGet(context, body);
                            } else {
                                context.fail(reply.cause());
                            }
                        });
    }

    private void setResponseForGet(RoutingContext context, JsonObject body) {
        setCorsHeaders(context);
        context.response().setStatusCode(200).end(Json.encodePrettily(body));
    }

    private void setCorsHeaders(RoutingContext context) {
        context.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .putHeader("Access-Control-Allow-Origin", "*")
                .putHeader("Access-Control-Request-Method", "GET, POST, PATCH, DELETE");
    }
}
