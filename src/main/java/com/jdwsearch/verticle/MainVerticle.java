package com.jdwsearch.verticle;

import com.jdwsearch.model.Product;
import com.jdwsearch.model.RankingConfig;
import com.jdwsearch.stub.SampleDataGenerator;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Future<Void> startFuture) {
        Future<String> dbVerticleDeployment = Future.future();
        vertx.deployVerticle(new ProductDBVerticle(), dbVerticleDeployment.completer());
        dbVerticleDeployment
                .compose(
                        id -> {
                            prepareSampleProductData();
                            prepareSampleRankingConfig();
                            Future<String> httpVerticleDeployment = Future.future();
                            vertx.deployVerticle(
                                    "com.jdwsearch.verticle.HttpServerVerticle",
                                    new DeploymentOptions().setInstances(2),
                                    httpVerticleDeployment.completer());
                            return httpVerticleDeployment;
                        })
                .setHandler(
                        ar -> {
                            if (ar.succeeded()) {
                                startFuture.complete();
                            } else {
                                startFuture.fail(ar.cause());
                            }
                        });
    }

    private void prepareSampleRankingConfig() {
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "db-check-ranking-config");
        String productDbQueue = "productdb.queue";
        vertx.eventBus()
             .send(
                     productDbQueue,
                     new JsonObject(),
                     options,
                     reply -> {
                         if (reply.succeeded()) {
                             if (reply.result().body().equals(false)) {
                                 LOGGER.info("No Config found... creating sample config for SimplyBe");
                                 RankingConfig sample = new RankingConfig("simply-be", 20, 20,20,20,20);
                                 createRankingConfig(sample);
                             } else {
                                 LOGGER.info("Sample RankingConfig Exist!");
                             }
                         }
                     });
    }

    private void prepareSampleProductData() {
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "db-check-product");
        String productDbQueue = "productdb.queue";
        vertx.eventBus()
                .send(
                        productDbQueue,
                        new JsonObject(),
                        options,
                        reply -> {
                            if (reply.succeeded()) {
                                if (reply.result().body().equals(false)) {
                                    LOGGER.info("No Products in the DB.. creating a few....!");
                                    SampleDataGenerator.getSampleData()
                                            .forEach(this::createProduct);
                                } else {
                                    LOGGER.info("Sample Products exist!");
                                }
                            }
                        });
    }

    private void createProduct(Product product) {
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "create-product");
        String productDbQueue = "productdb.queue";
        vertx.eventBus()
                .send(
                        productDbQueue,
                        JsonObject.mapFrom(product),
                        options,
                        reply -> {
                            if (reply.succeeded()) {
                                LOGGER.debug("Sample Products Created!");
                            } else {
                                LOGGER.debug("Sample Products creation failed!");
                            }
                        });
    }

    private void createRankingConfig(RankingConfig rankingConfig) {
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "create-ranking-config");
        String productDbQueue = "productdb.queue";
        vertx.eventBus()
             .send(
                     productDbQueue,
                     JsonObject.mapFrom(rankingConfig),
                     options,
                     reply -> {
                         if (reply.succeeded()) {
                             LOGGER.debug("Sample RankingConfig Created!");
                         } else {
                             LOGGER.debug("Sample RankingConfig creation failed!");
                         }
                     });
    }
}
