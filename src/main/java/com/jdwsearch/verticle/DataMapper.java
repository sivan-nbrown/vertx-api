package com.jdwsearch.verticle;

import com.jdwsearch.model.Product;
import com.jdwsearch.model.RankingConfig;
import com.jdwsearch.model.SqlQuery;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Properties;

public class DataMapper {
    public DataMapper() {
    }

    Product mapProduct(JsonArray result) {
        return new Product(
                result.getInteger(0),
                result.getString(1),
                result.getString(2),
                result.getBoolean(3),
                result.getDouble(4),
                result.getInteger(5),
                result.getDouble(6),
                result.getInteger(7),
                result.getInteger(8),
                result.getDouble(9),
                result.getDouble(10),
                result.getDouble(11),
                result.getDouble(12),
                result.getDouble(13));
    }

    RankingConfig mapRankingConfig(JsonArray result) {
        return new RankingConfig(
                result.getString(0),
                result.getDouble(1),
                result.getDouble(2),
                result.getDouble(3),
                result.getDouble(4),
                result.getDouble(5));
    }

    HashMap<SqlQuery, String> mapSqlQueries(Properties queriesProps, ProductDBVerticle productDBVerticle) {
        HashMap<SqlQuery, String> sqlQueries = new HashMap<>();

        sqlQueries.put(
                SqlQuery.CREATE_PRODUCT_TABLE, queriesProps.getProperty("create-product-table"));
        sqlQueries.put(SqlQuery.ALL_PRODUCTS, queriesProps.getProperty("all-products"));
        sqlQueries.put(SqlQuery.CREATE_PRODUCT, queriesProps.getProperty("create-product"));
        sqlQueries.put(SqlQuery.GET_PRODUCT, queriesProps.getProperty("get-product"));
        sqlQueries.put(SqlQuery.UPDATE_PRODUCT, queriesProps.getProperty("update-product"));
        sqlQueries.put(
                SqlQuery.UPDATE_PRODUCT_SCORE_ATTRIBUTES,
                queriesProps.getProperty("update-product-score-attributes"));
        sqlQueries.put(SqlQuery.DELETE_PRODUCT, queriesProps.getProperty("delete-product"));
        sqlQueries.put(SqlQuery.CHECK_DB_PRODUCT, queriesProps.getProperty("db-check-product"));
        sqlQueries.put(
                SqlQuery.CHECK_DB_RANKING_CONFIG,
                queriesProps.getProperty("db-check-ranking-config"));
        sqlQueries.put(
                SqlQuery.CREATE_RANKING_CONFIG_TABLE,
                queriesProps.getProperty("create-ranking-config-table"));
        sqlQueries.put(
                SqlQuery.ALL_RANKING_CONFIGS, queriesProps.getProperty("all-ranking-config"));
        sqlQueries.put(
                SqlQuery.CREATE_RANKING_CONFIG, queriesProps.getProperty("create-ranking-config"));
        sqlQueries.put(SqlQuery.GET_RANKING_CONFIG, queriesProps.getProperty("get-ranking-config"));
        sqlQueries.put(
                SqlQuery.UPDATE_RANKING_CONFIG, queriesProps.getProperty("update-ranking-config"));
        sqlQueries.put(
                SqlQuery.DELETE_RANKING_CONFIG, queriesProps.getProperty("delete-ranking-config"));

        return sqlQueries;
    }

    JsonArray mapProductToJsonArray(JsonObject request) {
        return new JsonArray()
            .add(request.getInteger("productId"))
            .add(request.getString("description"))
            .add(request.getString("imageUrl"))
            .add(request.getBoolean("inSale"))
            .add(request.getDouble("price"))
            .add(request.getInteger("qtySoldInLastDay"))
            .add(request.getDouble("margin"))
            .add(request.getInteger("views"))
            .add(request.getInteger("stock"))
            .add(request.getInteger("normalizedQtySoldInLastDay"))
            .add(request.getDouble("normalizedMargin"))
            .add(request.getInteger("normalizedViews"))
            .add(request.getInteger("normalizedStock"))
            .add(request.getDouble("score"));
    }
}