package com.jdwsearch.verticle;

import com.jdwsearch.model.Product;
import com.jdwsearch.model.RankingConfig;

import java.util.ArrayList;
import java.util.List;

class DataNormalizer {
    DataNormalizer() {}

    List<Product> normalizeProductAttributes(List<Product> products) {

        List<Product> normalizedProducts = new ArrayList<Product>();
        int minQtySold = products.stream().mapToInt(Product::getQtySoldInLastDay).min().orElse(0);

        int maxQtySold = products.stream().mapToInt(Product::getQtySoldInLastDay).max().orElse(0);

        double minMargin = products.stream().mapToDouble(Product::getMargin).min().orElse(0);

        double maxMargin = products.stream().mapToDouble(Product::getMargin).max().orElse(0);

        double minViews = products.stream().mapToInt(Product::getViews).min().orElse(0);

        double maxViews = products.stream().mapToInt(Product::getViews).max().orElse(0);

        double minStock = products.stream().mapToInt(Product::getStock).min().orElse(0);

        double maxStock = products.stream().mapToInt(Product::getStock).max().orElse(0);

        products.forEach(
                product -> {
                    product.setNormalizedQtySoldInLastDay(
                            normalize(product.getQtySoldInLastDay(), minQtySold, maxQtySold));
                    product.setNormalizedMargin(
                            normalize(product.getMargin(), minMargin, maxMargin));
                    product.setNormalizedStock(normalize(product.getStock(), minStock, maxStock));
                    product.setNormalizedViews(normalize(product.getViews(), minViews, maxViews));
                    normalizedProducts.add(product);
                });

        return normalizedProducts;
    }

    private double normalize(double x, double min, double max) {
        return ((x - min) / (max - min));
    }

    List<Product> scoreProducts(List<Product> normalizedProducts, RankingConfig config) {
        List<Product> scoredProducts = new ArrayList<>();
        normalizedProducts.forEach(
                product -> {
                    product.setScore(config.calculateScore(product));
                    scoredProducts.add(product);
                });
        return scoredProducts;
    }
}
