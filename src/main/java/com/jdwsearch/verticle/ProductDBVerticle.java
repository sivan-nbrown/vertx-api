package com.jdwsearch.verticle;

import com.jdwsearch.model.ErrorCodes;
import com.jdwsearch.model.Product;
import com.jdwsearch.model.RankingConfig;
import com.jdwsearch.model.SqlQuery;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class ProductDBVerticle extends AbstractVerticle {
    private static final String CONFIG_PRODUCT_DB_JDBC_URL = "productdb.jdbc.url";
    private static final String CONFIG_PRODUCT_DB_JDBC_DRIVER_CLASS = "productdb.jdbc.driver_class";
    private static final String CONFIG_PRODUCT_DB_JDBC_MAX_POOL_SIZE =
            "productdb.jdbc.max_pool_size";
    private static final String CONFIG_PRODUCT_DB_SQL_QUERIES_RESOURCE_FILE =
            "productdb.sqlqueries.resource.file";
    private static final String CONFIG_PRODUCT_DB_QUEUE = "productdb.queue";
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDBVerticle.class);
    private final DataNormalizer dataNormalizer = new DataNormalizer();
    private final DataMapper dataMapper = new DataMapper();
    private JDBCClient dbClient;
    private HashMap<SqlQuery, String> sqlQueries;

    private void loadSqlQueries() throws IOException {
        String queriesFile = config().getString(CONFIG_PRODUCT_DB_SQL_QUERIES_RESOURCE_FILE);
        InputStream queriesInputStream;
        if (queriesFile != null) {
            queriesInputStream = new FileInputStream(queriesFile);
        } else {
            queriesInputStream = getClass().getResourceAsStream("/db-queries.properties");
        }
        Properties queriesProps = new Properties();
        queriesProps.load(queriesInputStream);
        queriesInputStream.close();
        sqlQueries = dataMapper.mapSqlQueries(queriesProps, this);
    }

    public void start(Future<Void> startFuture) throws IOException {
        loadSqlQueries();
        dbClient =
                JDBCClient.createShared(
                        vertx,
                        new JsonObject()
                                .put(
                                        "url",
                                        config().getString(
                                                        CONFIG_PRODUCT_DB_JDBC_URL,
                                                        "jdbc:hsqldb:file:db/product"))
                                .put(
                                        "driver_class",
                                        config().getString(
                                                        CONFIG_PRODUCT_DB_JDBC_DRIVER_CLASS,
                                                        "org.hsqldb.jdbcDriver"))
                                .put(
                                        "max_pool_size",
                                        config().getInteger(
                                                        CONFIG_PRODUCT_DB_JDBC_MAX_POOL_SIZE, 30)));

        dbClient.getConnection(
                ar -> {
                    if (ar.failed()) {
                        LOGGER.error("Could not open a database connection", ar.cause());
                        startFuture.fail(ar.cause());
                    } else {
                        createTables(startFuture, ar);
                    }
                });
    }

    private void createTables(Future<Void> startFuture, AsyncResult<SQLConnection> ar) {
        SQLConnection connection = ar.result();
        connection.execute(
                sqlQueries.get(SqlQuery.CREATE_PRODUCT_TABLE),
                create -> {
                    if (create.succeeded()) {
                        createRankingConfigTable(startFuture, connection, create);
                    }
                    if (create.failed()) {
                        LOGGER.error("Database preparation error", create.cause());
                        startFuture.fail(create.cause());
                    } else {
                        vertx.eventBus()
                                .consumer(
                                        config().getString(
                                                        CONFIG_PRODUCT_DB_QUEUE, "productdb.queue"),
                                        this::onMessage);
                    }
                });
    }

    private void createRankingConfigTable(
            Future<Void> startFuture, SQLConnection connection, AsyncResult<Void> create) {
        connection.execute(
                sqlQueries.get(SqlQuery.CREATE_RANKING_CONFIG_TABLE),
                createConfigTable -> {
                    if (createConfigTable.succeeded()) {
                        connection.close();
                        startFuture.complete();
                    } else {
                        LOGGER.error("Database preparation error", create.cause());
                        startFuture.fail(create.cause());
                    }
                });
    }

    private void onMessage(Message<JsonObject> message) {
        if (!message.headers().contains("action")) {
            LOGGER.error(
                    "No action header specified for message with headers {} and body {}",
                    message.headers(),
                    message.body().encodePrettily());
            message.fail(ErrorCodes.NO_ACTION_SPECIFIED.ordinal(), "No action header specified");
            return;
        }
        String action = message.headers().get("action");
        switch (action) {
            case "db-check-product":
                checkDBProduct(message);
                break;
            case "db-check-ranking-config":
                checkDBRankingConfig(message);
                break;
            case "all-products":
                getAll(message);
                break;
            case "get-product":
                getProduct(message);
                break;
            case "create-product":
                createProduct(message);
                break;
            case "update-product":
                updateProduct(message);
                break;
            case "delete-product":
                deleteProduct(message);
                break;
            case "delete-all":
                deleteAll(message);
                break;
            case "all-ranking-config":
                getAllRankingConfigs(message);
                break;
            case "get-ranking-config":
                getRankingConfigForSite(
                        handlerForConfigReturn(message),
                        new JsonArray().add(message.body().getString("site")));
                break;
            case "create-ranking-config":
                createRankingConfig(message);
                break;
            case "update-ranking-config":
                updateRankingConfig(message);
                break;
            case "delete-ranking-config":
                deleteRankingConfig(message);
                break;
            default:
                message.fail(ErrorCodes.BAD_ACTION.ordinal(), "Bad action: " + action);
        }
    }

    private void deleteAll(Message<JsonObject> message) {
        dbClient.query(
                sqlQueries.get(SqlQuery.ALL_PRODUCTS),
                res -> {
                    if (res.succeeded()) {
                        res.result()
                                .getResults()
                                .forEach(
                                        item -> {
                                            Integer id = item.getInteger(0);
                                            dbClient.querySingleWithParams(
                                                    sqlQueries.get(SqlQuery.DELETE_PRODUCT),
                                                    new JsonArray().add(id),
                                                    del -> {
                                                        if (del.failed()) {
                                                            LOGGER.error(
                                                                    "Product deletion for Id {} failed!",
                                                                    id);
                                                        }
                                                    });
                                        });
                    }
                    message.reply("Ok");
                });
    }

    private void checkDBProduct(Message<JsonObject> message) {
        dbClient.query(
                sqlQueries.get(SqlQuery.CHECK_DB_PRODUCT),
                res -> {
                    if (res.succeeded()) {
                        message.reply(res.result().getResults().get(0).getInteger(0) > 0);
                    } else {
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void checkDBRankingConfig(Message<JsonObject> message) {
        dbClient.query(
                sqlQueries.get(SqlQuery.CHECK_DB_RANKING_CONFIG),
                res -> {
                    if (res.succeeded()) {
                        message.reply(res.result().getResults().get(0).getInteger(0) > 0);
                    } else {
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private Handler<AsyncResult<JsonArray>> handlerForConfigReturn(Message<JsonObject> message) {
        JsonArray params = new JsonArray().add(message.body().getString("site"));
        return res -> {
            if (res.succeeded()) {
                JsonArray result = res.result();
                RankingConfig found = dataMapper.mapRankingConfig(result);
                message.reply(JsonObject.mapFrom(found));
            } else {
                LOGGER.error("RankingConfig fetch for Id {} failed!", params.getInteger(0));
                reportQueryError(message, res.cause());
            }
        };
    }

    private void createProduct(Message<JsonObject> message) {
        JsonObject request = message.body();
        JsonArray data = dataMapper.mapProductToJsonArray(request);

        dbClient.updateWithParams(
                sqlQueries.get(SqlQuery.CREATE_PRODUCT),
                data,
                res -> {
                    if (res.succeeded()) {
                        message.reply("ok");
                    } else {
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void updateProduct(Message<JsonObject> message) {
        JsonObject body = message.body();
        JsonArray params = new JsonArray().add(body.getInteger("productId"));

        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.GET_PRODUCT),
                params,
                res -> {
                    if (res.succeeded()) {
                        JsonArray data =
                                new JsonArray()
                                        .add(body.getString("description"))
                                        .add(body.getString("imageUrl"))
                                        .add(body.getBoolean("inSale"))
                                        .add(body.getDouble("price"))
                                        .add(body.getInteger("productId"));

                        dbClient.updateWithParams(
                                sqlQueries.get(SqlQuery.UPDATE_PRODUCT),
                                data,
                                updateResult -> {
                                    if (res.succeeded()) {
                                        message.reply("ok");
                                    } else {
                                        reportQueryError(message, updateResult.cause());
                                    }
                                });
                    } else {
                        LOGGER.error("Product fetch for Id {} failed!", params.getInteger(0));
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void deleteProduct(Message<JsonObject> message) {
        JsonArray params = new JsonArray().add(message.body().getInteger("productId"));
        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.DELETE_PRODUCT),
                params,
                res -> {
                    if (res.succeeded()) {
                        message.reply(Json.encodePrettily(new JsonObject().put("Success", "true")));
                    } else {
                        LOGGER.error("Product deletion for Id {} failed!", params.getInteger(0));
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void getAll(Message<JsonObject> message) {
        dbClient.query(
                sqlQueries.get(SqlQuery.ALL_PRODUCTS),
                prodRes -> {
                    List<Product> products = new ArrayList<>();
                    if (prodRes.succeeded()) {
                        buildProducts(prodRes, products);
                        List<Product> sortedProducts =
                                products.stream()
                                        .sorted(
                                                Comparator.comparingDouble(Product::getScore)
                                                        .reversed())
                                        .collect(Collectors.toList());
                        JsonArray result = new JsonArray();
                        sortedProducts.forEach(r -> result.add(JsonObject.mapFrom(r)));
                        message.reply(new JsonObject().put("products", result));
                    } else {
                        LOGGER.error("Product fetch failed!");
                        reportQueryError(message, prodRes.cause());
                    }
                    new JsonArray().add(message.body().getString("site"));
                });
    }

    private void getProduct(Message<JsonObject> message) {
        JsonArray params = new JsonArray().add(message.body().getInteger("productId"));
        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.GET_PRODUCT),
                params,
                res -> {
                    if (res.succeeded()) {
                        JsonArray result = res.result();
                        Product found = dataMapper.mapProduct(result);
                        message.reply(JsonObject.mapFrom(found));
                    } else {
                        LOGGER.error("Product fetch for Id {} failed!", params.getInteger(0));
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void createRankingConfig(Message<JsonObject> message) {
        JsonObject request = message.body();

        RankingConfig config = request.mapTo(RankingConfig.class);

        JsonArray data =
                new JsonArray()
                        .add(request.getString("site"))
                        .add(request.getDouble("inSale"))
                        .add(request.getDouble("qtySoldInLastDay"))
                        .add(request.getDouble("margin"))
                        .add(request.getDouble("views"))
                        .add(request.getDouble("stock"));

        dbClient.updateWithParams(
                sqlQueries.get(SqlQuery.CREATE_RANKING_CONFIG),
                data,
                res -> {
                    if (res.succeeded()) {
                        normalizeAndRankProducts(config);
                        message.reply("ok");
                    } else {
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void normalizeAndRankProducts(RankingConfig config) {
        dbClient.query(
                sqlQueries.get(SqlQuery.ALL_PRODUCTS),
                prodRes -> {
                    List<Product> products = new ArrayList<>();
                    if (prodRes.succeeded()) {
                        buildProducts(prodRes, products);

                        List<Product> normalizedProducts =
                                dataNormalizer.normalizeProductAttributes(products);

                        List<Product> scoredProducts =
                                dataNormalizer.scoreProducts(normalizedProducts, config);

                        scoredProducts.forEach(
                                product -> {
                                    JsonArray data =
                                            new JsonArray()
                                                    .add(product.getNormalizedQtySoldInLastDay())
                                                    .add(product.getNormalizedMargin())
                                                    .add(product.getNormalizedViews())
                                                    .add(product.getNormalizedStock())
                                                    .add(product.getScore())
                                                    .add(product.getProductId());

                                    dbClient.queryWithParams(
                                            sqlQueries.get(
                                                    SqlQuery.UPDATE_PRODUCT_SCORE_ATTRIBUTES),
                                            data,
                                            updateProdRes -> {
                                                if (updateProdRes.succeeded()) {
                                                    LOGGER.debug("well");
                                                }
                                            });
                                });
                    }
                });
    }

    private void buildProducts(AsyncResult<ResultSet> prodRes, List<Product> products) {
        List<JsonArray> results = prodRes.result().getResults();
        results.forEach(result -> products.add(dataMapper.mapProduct(result)));
    }

    private void getAllRankingConfigs(Message<JsonObject> message) {
        dbClient.query(
                sqlQueries.get(SqlQuery.ALL_RANKING_CONFIGS),
                res -> {
                    if (res.succeeded()) {
                        List<JsonArray> results = res.result().getResults();
                        List<RankingConfig> configs = new ArrayList<>();
                        results.forEach(result -> configs.add(dataMapper.mapRankingConfig(result)));
                        JsonArray result = new JsonArray();
                        configs.forEach(r -> result.add(JsonObject.mapFrom(r)));
                        message.reply(new JsonObject().put("ranking-configs", result));
                    } else {
                        LOGGER.error("Config fetch failed!");
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void getRankingConfigForSite(
            Handler<AsyncResult<JsonArray>> handler, JsonArray params) {
        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.GET_RANKING_CONFIG), params, handler);
    }

    private void updateRankingConfig(Message<JsonObject> message) {
        JsonObject body = message.body();
        JsonArray params = new JsonArray().add(body.getString("site"));

        RankingConfig config = body.mapTo(RankingConfig.class);

        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.GET_RANKING_CONFIG),
                params,
                res -> {
                    if (res.succeeded()) {
                        JsonArray data =
                                new JsonArray()
                                        .add(body.getDouble("inSale"))
                                        .add(body.getDouble("qtySoldInLastDay"))
                                        .add(body.getDouble("margin"))
                                        .add(body.getDouble("views"))
                                        .add(body.getDouble("stock"))
                                        .add(body.getString("site"));

                        dbClient.updateWithParams(
                                sqlQueries.get(SqlQuery.UPDATE_RANKING_CONFIG),
                                data,
                                updateResult -> {
                                    if (res.succeeded()) {
                                        normalizeAndRankProducts(config);
                                        message.reply("ok");
                                    } else {
                                        reportQueryError(message, updateResult.cause());
                                    }
                                });
                    } else {
                        LOGGER.error("RankingConfig fetch for Id {} failed!", params.getInteger(0));
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void deleteRankingConfig(Message<JsonObject> message) {
        JsonArray params = new JsonArray().add(message.body().getInteger("site"));
        dbClient.querySingleWithParams(
                sqlQueries.get(SqlQuery.DELETE_RANKING_CONFIG),
                params,
                res -> {
                    if (res.succeeded()) {
                        message.reply(Json.encodePrettily(new JsonObject().put("Success", "true")));
                    } else {
                        LOGGER.error(
                                "RankingConfig deletion for Site {} failed!", params.getInteger(0));
                        reportQueryError(message, res.cause());
                    }
                });
    }

    private void reportQueryError(Message<JsonObject> message, Throwable cause) {
        LOGGER.error("Database query error", cause);
        message.fail(ErrorCodes.DB_ERROR.ordinal(), cause.getMessage());
    }
}
