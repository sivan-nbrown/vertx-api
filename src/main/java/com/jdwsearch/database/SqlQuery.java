package com.jdwsearch.database;

public enum SqlQuery {
        CHECK_DB,
        CREATE_PRODUCT_TABLE,
        ALL_PRODUCTS,
        GET_PRODUCT,
        CREATE_PRODUCT,
        UPDATE_PRODUCT,
        DELETE_PRODUCT,
    }