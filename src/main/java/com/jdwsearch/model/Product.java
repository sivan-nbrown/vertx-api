package com.jdwsearch.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Product {
    private int productId;

    private String description;

    private String imageUrl;

    private boolean inSale;

    private double price;

    private int qtySoldInLastDay;

    private double margin;

    private int views;

    private int stock;

    private double normalizedQtySoldInLastDay;

    private double normalizedMargin;

    private double normalizedViews;

    private double normalizedStock;

    private double score;

    public Product(
            int productId,
            String description,
            String imageUrl,
            boolean inSale,
            double price,
            int qtySoldInLastDay,
            double margin,
            int views,
            int stock,
            double normalizedQtySoldInLastDay,
            double normalizedMargin,
            double normalizedViews,
            double normalizedStock,
            double score) {
        this.productId = productId;
        this.description = description;
        this.imageUrl = imageUrl;
        this.inSale = inSale;
        this.price = price;
        this.qtySoldInLastDay = qtySoldInLastDay;
        this.margin = margin;
        this.views = views;
        this.stock = stock;
        this.normalizedQtySoldInLastDay = normalizedQtySoldInLastDay;
        this.normalizedMargin = normalizedMargin;
        this.normalizedViews = normalizedViews;
        this.normalizedStock = normalizedStock;
        this.score = score;
    }

    public Product(
            int productId, String description, String imageUrl, boolean inSale, double price) {
        this.productId = productId;
        this.description = description;
        this.imageUrl = imageUrl;
        this.inSale = inSale;
        this.price = price;
    }

    public Product() {}

    public double getNormalizedQtySoldInLastDay() {
        return normalizedQtySoldInLastDay;
    }

    public double getNormalizedMargin() {
        return normalizedMargin;
    }

    public double getNormalizedViews() {
        return normalizedViews;
    }

    public double getNormalizedStock() {
        return normalizedStock;
    }

    public int getProductId() {
        return productId;
    }

    public double getScore() {
        return score;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isInSale() {
        return inSale;
    }

    public double getPrice() {
        return price;
    }

    public int getQtySoldInLastDay() {
        return qtySoldInLastDay;
    }

    public double getMargin() {
        return margin;
    }

    public int getViews() {
        return views;
    }

    public int getStock() {
        return stock;
    }

    public void setNormalizedQtySoldInLastDay(double normalizedQtySoldInLastDay) {
        this.normalizedQtySoldInLastDay = normalizedQtySoldInLastDay;
    }

    public void setNormalizedMargin(double normalizedMargin) {
        this.normalizedMargin = normalizedMargin;
    }

    public void setNormalizedViews(double normalizedViews) {
        this.normalizedViews = normalizedViews;
    }

    public void setNormalizedStock(double normalizedStock) {
        this.normalizedStock = normalizedStock;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
