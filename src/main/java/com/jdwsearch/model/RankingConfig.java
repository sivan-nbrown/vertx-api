package com.jdwsearch.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class RankingConfig {

    private String site;

    private double inSale;

    private double qtySoldInLastDay;

    private double margin;

    private double views;

    private double stock;

    public RankingConfig() {}

    public RankingConfig(
            String site,
            double inSale,
            double qtySoldInLastDay,
            double margin,
            double views,
            double stock) {
        this.site = site;
        this.inSale = inSale;
        this.qtySoldInLastDay = qtySoldInLastDay;
        this.margin = margin;
        this.views = views;
        this.stock = stock;
    }

    public double calculateScore(Product product) {
        return this.inSale
                + this.qtySoldInLastDay * product.getNormalizedQtySoldInLastDay()
                + this.margin * product.getNormalizedMargin()
                + this.views * product.getNormalizedViews()
                + this.stock * product.getNormalizedStock();
    }
}
